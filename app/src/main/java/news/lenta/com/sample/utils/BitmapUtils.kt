package news.lenta.com.sample.utils

import android.content.res.Resources
import android.graphics.Bitmap


object BitmapUtils {

    private fun getImageFactor(resources: Resources): Float {
        val metrics = resources.displayMetrics
        return metrics.density / 3f
    }

    fun createNotificationBitmap(resources: Resources, bitmap: Bitmap): Bitmap {
        val multiplier = getImageFactor(resources)
        return Bitmap.createScaledBitmap(bitmap, (bitmap.width * multiplier).toInt(), (bitmap.height * multiplier).toInt(), false)
    }
}