package news.lenta.com.sample.ui.fragments.news

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import news.lenta.com.sample.databinding.ItemNewsBinding
import news.lenta.com.sample.ui.model.MediaEntity
import news.lenta.com.sample.ui.model.NewsEntity
import kotlin.properties.Delegates

//created by power on 21.10.17

class NewsListAdapter(private val onItemClickListener: NewsListAdapter.OnItemClickListener)
    : RecyclerView.Adapter<NewsListAdapter.ViewHolder>() {
    private var items: MutableList<NewsEntity> = mutableListOf()
    lateinit var binding: ItemNewsBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        binding = ItemNewsBinding.inflate(inflater, parent, false)
        return ViewHolder(binding.root)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.item = items[position]
        holder.binding.root.setOnClickListener({
            items[position].let { onItemClickListener.onClick(position, it)}})
    }


    override fun getItemCount(): Int = items.size

    fun addItem(entry: NewsEntity) {
        items.add(entry)
    }

    fun replaceItems(news: List<NewsEntity>) {
        items = news.toMutableList()
        notifyDataSetChanged()
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var binding: ItemNewsBinding = DataBindingUtil.bind(itemView)!!
    }

    interface OnItemClickListener {
        fun onClick(pos: Int, item: NewsEntity)
    }

}