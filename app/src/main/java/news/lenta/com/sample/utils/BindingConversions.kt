package news.lenta.com.sample.utils

import android.databinding.BindingAdapter
import android.graphics.Bitmap
import android.support.v7.widget.AppCompatImageView
import com.squareup.picasso.Picasso
import news.lenta.com.sample.R


object BindingConversions {
    @JvmStatic
    @BindingAdapter("android:picassoSrc")
    fun loadImage(imageView: AppCompatImageView, url: String?) {
        url?.let {
            if (!it.isEmpty()) {
                Picasso.with(imageView.context)
                        .load(it)
                        .error(R.drawable.place_holder)
                        .into(imageView)
            }
        }
    }

//    @JvmStatic
//    @BindingAdapter("android:bitmap")
//    fun loadBitmap(imageView: AppCompatImageView, bitmap: Bitmap?) {
//        bitmap?.let {
//                Picasso.with(imageView.context)
//                        .load(it)
//                        .error(R.drawable.place_holder)
//                        .into(imageView)
//        }
//    }
}