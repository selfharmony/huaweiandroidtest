package news.lenta.com.sample.ui.fragments.detail

import news.lenta.com.sample.ui.base.BaseContract

interface Contract {
    interface view : BaseContract.View {

    }

    interface presenter : BaseContract.Presenter {

    }
}