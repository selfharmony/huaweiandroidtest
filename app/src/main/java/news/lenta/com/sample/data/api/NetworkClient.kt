package news.lenta.com.sample.data.api

import android.graphics.Bitmap
import io.reactivex.Flowable
import news.lenta.com.sample.data.api.model.NewsResponse
import okhttp3.ResponseBody


interface NetworkClient {

    fun getNews(): Flowable<NewsResponse>
    fun getNotificationIcon(url: String): Flowable<ResponseBody>
}