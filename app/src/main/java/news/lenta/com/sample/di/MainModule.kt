package news.lenta.com.sample.di

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import android.content.SharedPreferences
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import news.lenta.com.sample.BuildConfig
import news.lenta.com.sample.core.API_ENDPOINT
import news.lenta.com.sample.core.APP_PREFERENCES
import news.lenta.com.sample.data.api.NetworkClient
import news.lenta.com.sample.data.api.ProductionNetworkClient
import news.lenta.com.sample.data.repository.ProductionRepository
import news.lenta.com.sample.data.repository.Repository
import news.lenta.com.sample.data.storage.LocalStorage
import news.lenta.com.sample.data.storage.ProductionLocalStorage
import news.lenta.com.sample.data.storage.room.DataBase
import news.lenta.com.sample.data.storage.room.dao.NewsDao
import news.lenta.com.sample.domain.interactors.ThreadScheduler
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.Executor
import javax.inject.Named
import javax.inject.Singleton

@dagger.Module
class MainModule(val app: Application) {

    companion object {
        private const val DB_NAME = "room_db"
    }


    @Singleton
    @Provides
    fun provideWorkerThread(): Executor {
        return ThreadScheduler()
    }

    @Singleton
    @Provides
    fun provideUiThread(): Scheduler {
        return AndroidSchedulers.mainThread()
    }

    @Singleton
    @Provides
    fun provideRepository(networkClient: NetworkClient, localStorage: LocalStorage): Repository {
        return ProductionRepository(networkClient, localStorage)
    }


    @Singleton
    @Provides
    fun provideNetworkClient(@Named("baseUrl") baseUrl: String, level: HttpLoggingInterceptor.Level): NetworkClient {
        return ProductionNetworkClient(baseUrl, level)
    }

    @Singleton
    @Provides
    fun provideLocalStorage(prefs: SharedPreferences, newsDao: NewsDao): LocalStorage {
        return ProductionLocalStorage(prefs, newsDao)
    }

    @Singleton
    @Provides
    fun provideNewsDao(database: DataBase): NewsDao = database.getNewsDao()

    @Singleton
    @Provides
    internal fun provideDataBase(): DataBase {
        return Room.databaseBuilder(
                app.applicationContext,
                DataBase::class.java,
                DB_NAME
        ).fallbackToDestructiveMigration()
                .build()
    }

    @Singleton
    @Provides
    fun provideLevel(): HttpLoggingInterceptor.Level {
        return if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
    }

    @Provides
    @Singleton
    fun providesSharedPreferences(): SharedPreferences {
        return app.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE)
    }


    @Singleton
    @Provides
    @Named("baseUrl")
    fun provideBaseUrl(): String {
        return API_ENDPOINT
    }

}