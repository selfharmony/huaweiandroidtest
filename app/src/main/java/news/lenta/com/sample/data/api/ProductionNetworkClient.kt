package news.lenta.com.sample.data.api

import android.graphics.Bitmap
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import news.lenta.com.sample.data.api.model.NewsResponse
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.simplexml.SimpleXmlConverterFactory


class ProductionNetworkClient(baseUrl: String, level: HttpLoggingInterceptor.Level): NetworkClient {

    private var api: Api

    init {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = level
        val client = OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build()
        val retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(SimpleXmlConverterFactory.createNonStrict())
                .client(client)
                .build()
        api = retrofit.create(Api::class.java)
    }


    override fun getNews(): Flowable<NewsResponse> {
        return api.getNews().toFlowable(BackpressureStrategy.LATEST)
    }

    override fun getNotificationIcon(url: String): Flowable<ResponseBody> {
        return api.getImage(url).toFlowable(BackpressureStrategy.LATEST)
    }
}