package news.lenta.com.sample.ui.base

interface BaseContract{
    interface View{
        fun showToast(message: String)
        fun showLoading()
        fun hideLoading()
    }

    interface Presenter{

    }
}