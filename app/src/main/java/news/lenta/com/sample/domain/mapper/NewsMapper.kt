package news.lenta.com.sample.domain.mapper

import android.graphics.Bitmap
import news.lenta.com.sample.data.api.model.NewsResponse
import news.lenta.com.sample.data.storage.room.model.NewsForDb
import news.lenta.com.sample.ui.model.NewsEntity
import kotlin.collections.ArrayList


object NewsMapper {

    fun fromNetwork(source: NewsResponse?): List<NewsEntity> {
        source?.let {
            val mappedNews = ArrayList<NewsEntity>()

            it.channel?.item?.mapNotNullTo(mappedNews) {
                NewsEntity(
                        title = it?.title ?: "No Title",
                        summary = it?.description ?: "",
                        articleUrl = it?.link ?: "",
                        publishedDate = it?.pubDate ?: "",
                        imageUrl = it?.enclosure?.url ?: ""
                )
            }
            return mappedNews
        } ?: throw Exception("NewsResponse is null")
    }

    fun toDb(source: List<NewsEntity>): List<NewsForDb> {
        source.let {
            val mappedNews = ArrayList<NewsForDb>()

            it.mapTo(mappedNews) {
                NewsForDb(
                        id = 0,
                        title = it.title,
                        summary = it.summary,
                        articleUrl = it.articleUrl,
                        publishedDate = it.publishedDate,
                        imageUrl = it.imageUrl
                )
            }
            return mappedNews
        }
    }



    fun fromDb(source: List<NewsForDb>): List<NewsEntity> {
        source.let {
            val mappedNews = ArrayList<NewsEntity>()

            it.mapTo(mappedNews) {
                NewsEntity(
                        title = it.title,
                        summary = it.summary,
                        articleUrl = it.articleUrl,
                        publishedDate = it.publishedDate,
                        imageUrl = it.imageUrl
                )
            }
            return mappedNews
        }
    }

}