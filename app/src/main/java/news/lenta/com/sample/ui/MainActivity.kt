package news.lenta.com.sample.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import news.lenta.com.sample.R
import news.lenta.com.sample.ui.base.BaseFragment
import news.lenta.com.sample.ui.fragments.detail.DetailFragment
import news.lenta.com.sample.ui.fragments.news.NewsListFragment
import news.lenta.com.sample.utils.*
import android.content.res.Configuration
import news.lenta.com.sample.core.NOTIFICATION_INTENT_EXTRA


class MainActivity : AppCompatActivity(), Router, FragmentsUiManager {

    var newsEntityBundle: Bundle? = null
    private val TAG = "MainActivity"
    val fm: FragmentManager by lazy { supportFragmentManager }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initToolbar()
        fm.addOnBackStackChangedListener {
            val topFragment = fm.findFragmentById(R.id.main_container)
            when (topFragment) {
                is BaseFragment -> {
                    topFragment.view?.requestFocus()
                    isFullScreen(topFragment.isFullScreen())
                }
            }
        }
        navigateToNews()

    }


    override fun onBackPressed() {
        if (onBackPressedInTopFragment())
            super.onBackPressed()
    }

    private fun onBackPressedInTopFragment(): Boolean {
        val topFragment = fm.findFragmentById(R.id.main_container)
        return (topFragment as? BaseFragment)?.onBackPressedCallback() ?: true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val id = item.itemId


        return if (id == R.id.action_settings) {
            true
        } else super.onOptionsItemSelected(item)

    }

    private fun initToolbar() {
        val toolbar = toolbar
        setSupportActionBar(toolbar)
    }


    private fun replaceTransaction(fragment: BaseFragment): FragmentTransaction {
        val transaction = fm.beginTransaction()
        val topFragment = fm.findFragmentById(R.id.main_container) as? BaseFragment
        if (topFragment?.javaClass == fragment) {
            return transaction
        } else {
            transaction.replace(R.id.main_container, fragment, fragment.TAG)
            isFullScreen(fragment.isFullScreen())
            println(fragment.TAG)
            return transaction
        }
    }


    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
    }

    private fun addTransaction(fragment: BaseFragment): FragmentTransaction {
        isFullScreen(fragment.isFullScreen())
        return fm.beginTransaction()
                .add(R.id.main_container, fragment, fragment.TAG)
                .addToBackStack(null)
    }

    //#UiManager
    override fun isFullScreen(fullScreenMode: Boolean) {
        if (fullScreenMode) {
            supportActionBar?.hide()
        } else {
            supportActionBar?.show()
        }
    }

    override fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun showLoading() {
        mainProgressBar.show()
    }

    override fun hideLoading() {
        mainProgressBar.hide()
    }

    //#Router
    override fun navigateToNews() {
        replaceTransaction(NewsListFragment.newInstance())
                .commit()
    }

    override fun navigateToDetail(bundle: Bundle) {
        addTransaction(DetailFragment.newInstance(bundle)).commit()
    }

    override fun openUrl(url: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        startActivity(intent)
    }

}

interface Router {
    fun navigateToNews()
    fun navigateToDetail(bundle: Bundle)
    fun openUrl(url: String)
}

interface FragmentsUiManager {
    fun showToast(message: String)
    fun isFullScreen(fullScreenMode: Boolean)
    fun showLoading()
    fun hideLoading()
//    fun showOrderDialog(message: String)
}