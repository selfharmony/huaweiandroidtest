package news.lenta.com.sample.data.storage.room.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import news.lenta.com.sample.data.storage.room.Tables

@Entity(tableName = Tables.NEWS)
class NewsForDb(
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "id")
        var id: Int,

        @ColumnInfo(name = "title")
        var title: String,

        @ColumnInfo(name = "summary")
        var summary: String,

        @ColumnInfo(name = "articleUrl")
        var articleUrl: String,

        @ColumnInfo(name = "publishedDate")
        var publishedDate: String,

        @ColumnInfo(name = "imageUrl")
        var imageUrl: String
)