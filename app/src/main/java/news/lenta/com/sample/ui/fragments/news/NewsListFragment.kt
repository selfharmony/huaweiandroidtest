package news.lenta.com.sample.ui.fragments.news

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.Provides
import io.reactivex.Scheduler
import kotlinx.android.synthetic.main.fragment_news_list.*
import news.lenta.com.sample.core.App
import news.lenta.com.sample.data.repository.Repository
import news.lenta.com.sample.data.service.NewsNotificationEngine
import news.lenta.com.sample.databinding.FragmentNewsListBinding
import news.lenta.com.sample.di.PerFragment
import news.lenta.com.sample.domain.interactors.GetNewsInteractor
import news.lenta.com.sample.domain.interactors.GetNewsUpdateInteractor
import news.lenta.com.sample.domain.interactors.Interactor
import news.lenta.com.sample.ui.Router
import news.lenta.com.sample.ui.base.BaseFragment
import news.lenta.com.sample.ui.model.NewsEntity
import news.lenta.com.sample.ui.model.NewsUpdateEntity
import java.util.concurrent.Executor
import javax.inject.Inject


class NewsListFragment : BaseFragment(), Contract.view {
    @Inject
    lateinit var presenter: NewsListPresenter
    lateinit var component: Component
    private val newsAdapter by lazy {
        NewsListAdapter(object : NewsListAdapter.OnItemClickListener {
            override fun onClick(pos: Int, item: NewsEntity) {
                presenter.onItemClicked(pos, item)
            }
        })
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject()
        presenter.view = this
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentNewsListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setView()
        presenter.getNews()
    }

    override fun onDestroy() {
        NewsListPresenter.savePresenter(presenter)
        super.onDestroy()
    }

    private fun setView() {
        list.adapter = newsAdapter
        list.layoutManager = LinearLayoutManager(context)
    }

    //region Dagger init
    fun inject() {
        val appComponent = (activity?.application as App).appComponent
        component = DaggerNewsListFragment_Component.builder().module(Module()).component(appComponent).build()
        component.inject(this)
    }

    @PerFragment
    @dagger.Component(
            modules = [(Module::class)],
            dependencies = [(App.Component::class)]
    )
    interface Component : App.Component {
        fun inject(fragment: NewsListFragment)
        fun presenter(): NewsListPresenter
    }

    @dagger.Module
    class Module {
        @Provides
        @PerFragment
        fun provideNewsFragmentPresenter(
                getNewsInteractor: Interactor<Void, List<NewsEntity>>,
                getNewsUpdateInteractor: Interactor<Void, List<NewsUpdateEntity>>
        ): NewsListPresenter {
            return NewsListPresenter.getSavedPresenter()
                    ?: NewsListPresenter(getNewsInteractor, getNewsUpdateInteractor)
        }

        @Provides
        @PerFragment
        fun provideGetNewsInteractor(executor: Executor, scheduler: Scheduler, repo: Repository): Interactor<Void, List<NewsEntity>> {
            return GetNewsInteractor(executor, scheduler, repo)
        }

        @Provides
        @PerFragment
        fun provideGetNewsUpdatesInteractor(executor: Executor, scheduler: Scheduler, repo: Repository): Interactor<Void, List<NewsUpdateEntity>> {
            return GetNewsUpdateInteractor(executor, scheduler, repo)
        }
    }

    //endregion

    override fun showNews(news: List<NewsEntity>) {
        newsAdapter.replaceItems(news)
        presenter.scheduleJob()
    }


    override fun navigateToDetail(bundle: Bundle) {
        (activity as Router).navigateToDetail(bundle);
    }

    companion object {
        val TAG = "NewsListFragment"

        fun newInstance(): NewsListFragment {
            val args = Bundle()
            val fragment = NewsListFragment()
            fragment.arguments = args
            return fragment
        }

    }


}