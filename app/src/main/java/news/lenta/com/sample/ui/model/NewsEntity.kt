package news.lenta.com.sample.ui.model

import android.graphics.Bitmap
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * This represents a news item
 */
@Parcelize
data class NewsEntity(
        var title: String,
        var summary: String,
        var articleUrl: String,
        var publishedDate: String,
        var imageUrl: String
) : Parcelable {

    fun getMediaUrl(): String? {
        return imageUrl
    }

    companion object {
        private val TAG = NewsEntity::class.java.simpleName
    }
}

data class NewsUpdateEntity(
        var title: String,
        var summary: String,
        var articleUrl: String,
        var publishedDate: String,
        var imageUrl: String,
        var notificationIcon: Bitmap
)