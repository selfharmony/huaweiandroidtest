package news.lenta.com.sample.ui.fragments.news

import android.os.Bundle
import com.evernote.android.job.JobManager
import io.reactivex.subscribers.DisposableSubscriber
import news.lenta.com.sample.data.service.NewsJobCreator
import news.lenta.com.sample.domain.interactors.Interactor
import news.lenta.com.sample.ui.base.BasePresenter
import news.lenta.com.sample.ui.model.NewsEntity
import news.lenta.com.sample.ui.model.NewsUpdateEntity
import news.lenta.com.sample.data.service.NewsSyncJob

class NewsListPresenter(
        private val getNewsInteractor: Interactor<Void, List<NewsEntity>>,
        private val getNewsUpdatesInteractor: Interactor<Void, List<NewsUpdateEntity>>
) : BasePresenter<Contract.view>(), Contract.presenter {

    var news: List<NewsEntity>? = null
    override fun onStart() {}
    override fun onStop() {}

    override fun getNews() {
        news?.let {
            view?.showNews(it)
        } ?: downloadNews()
    }

    private fun downloadNews() {
        view?.showLoading()
        getNewsInteractor.execute(object : DisposableSubscriber<List<NewsEntity>>() {
            override fun onComplete() {}

            override fun onNext(n: List<NewsEntity>) {
                view?.hideLoading()
                news = n
                view?.showNews(n)
            }

            override fun onError(t: Throwable?) {
                view?.hideLoading()
                t?.printStackTrace()
            }
        })
    }


    override fun scheduleJob() {
        val jobCreator = NewsJobCreator(getNewsUpdatesInteractor)
        JobManager.instance().addJobCreator(jobCreator)
        NewsSyncJob.scheduleJob()
    }

    override fun onItemClicked(pos: Int, item: NewsEntity) {
        val arguments = Bundle()
        val storyURL = arguments.putString("storyURL", item.articleUrl)
        val title = arguments.putString("title", item.title)
        val summary = arguments.putString("summary", item.summary)
        val imageURL = arguments.putString("imageURL", item.imageUrl)
        view?.navigateToDetail(arguments)
    }

    companion object {
        var storedPresenter: NewsListPresenter? = null

        fun savePresenter(presenter: NewsListPresenter?) {
            storedPresenter = presenter
        }

        fun getSavedPresenter(): NewsListPresenter? {
            return storedPresenter
        }
    }
}