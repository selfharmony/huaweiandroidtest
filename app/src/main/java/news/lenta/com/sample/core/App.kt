package news.lenta.com.sample.core

import android.app.Application
import io.reactivex.Scheduler
import news.lenta.com.sample.data.repository.Repository
import news.lenta.com.sample.di.MainModule
import java.util.concurrent.Executor
import javax.inject.Singleton
import com.evernote.android.job.JobManager


class App : Application(){
    var appComponent: Component? = null

    override fun onCreate() {
        super.onCreate()
        appComponent = createAppComponent()
        JobManager.create(this)
    }


    private fun createAppComponent(): Component {
        return DaggerApp_Component.builder().mainModule((MainModule(this))).build()
    }

    @Singleton
    @dagger.Component(modules = arrayOf(MainModule::class))
    interface Component {
        fun repo(): Repository
        fun workerThread(): Executor
        fun uiThread(): Scheduler
    }
}