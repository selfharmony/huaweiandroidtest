package news.lenta.com.sample.data.service

import com.evernote.android.job.Job
import com.evernote.android.job.JobRequest
import io.reactivex.subscribers.DisposableSubscriber
import news.lenta.com.sample.domain.interactors.Interactor
import news.lenta.com.sample.ui.model.NewsUpdateEntity

import java.util.concurrent.TimeUnit

class NewsSyncJob(
        val updateNewsInteractor: Interactor<Void, List<NewsUpdateEntity>>
) : Job() {
    val notifications = NewsNotificationEngine()

    override fun onRunJob(params: Job.Params): Job.Result {
        checkNewsUpdates()
        return Job.Result.SUCCESS
    }

    fun checkNewsUpdates() {
        updateNewsInteractor.execute(object : DisposableSubscriber<List<NewsUpdateEntity>>(){
            override fun onComplete() {}

            override fun onNext(newsUpdates: List<NewsUpdateEntity>?) {
                context.let {context ->
                    newsUpdates?.forEach { newsItem -> //показывает одну самую первую нотификацию
                        notifications.showNotification(newsItem, context)
                    }
                }
            }

            override fun onError(t: Throwable?) {
                t?.printStackTrace()
            }
        })
    }

    companion object {

        val TAG = "update_news_tag"

        fun scheduleJob() {
            JobRequest.Builder(NewsSyncJob.TAG)
                    .setPeriodic(TimeUnit.MINUTES.toMillis(15), TimeUnit.MINUTES.toMillis(5))
                    .setUpdateCurrent(true)
                    .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                    .setRequirementsEnforced(true)
                    .build()
                    .schedule()
            JobRequest.Builder(NewsSyncJob.TAG)
            .setExecutionWindow(3_000L, 15_000L)
                    .build()
                    .schedule();
        }
    }
}