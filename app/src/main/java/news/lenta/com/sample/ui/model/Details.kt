package news.lenta.com.sample.ui.model

data class Details(
var title: String?,
var summary: String?,
var storyURL: String?,
var imageURL: String?
)