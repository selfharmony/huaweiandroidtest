package news.lenta.com.sample.data.storage.room

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import news.lenta.com.sample.data.storage.room.dao.NewsDao
import news.lenta.com.sample.data.storage.room.model.NewsForDb


@Database(
        entities = [NewsForDb::class],
        version = 1
)
abstract class DataBase : RoomDatabase() {
    internal abstract fun getNewsDao(): NewsDao
}