package news.lenta.com.sample.data.service

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import news.lenta.com.sample.R
import news.lenta.com.sample.core.NOTIFICATION_INTENT_EXTRA
import news.lenta.com.sample.ui.MainActivity
import news.lenta.com.sample.ui.model.NewsUpdateEntity
import java.util.*
import android.app.PendingIntent



class NewsNotificationEngine {

    fun showNotification(newsUpdateEntity: NewsUpdateEntity, context: Context) {
        val notificationIntent = Intent(context, MainActivity::class.java)
        val arguments = Bundle()
        val storyURL = arguments.putString("storyURL", newsUpdateEntity.articleUrl)
        val title = arguments.putString("title", newsUpdateEntity.title)
        val summary = arguments.putString("summary", newsUpdateEntity.summary)
        val imageURL = arguments.putString("imageURL", newsUpdateEntity.imageUrl)

        notificationIntent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
        notificationIntent.putExtra(NOTIFICATION_INTENT_EXTRA, arguments)

        val stackBuilder = TaskStackBuilder.create(context)
        stackBuilder.addParentStack(MainActivity::class.java)
        stackBuilder.addNextIntent(notificationIntent);

        val pendingIntent = stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT
        )
//        val pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        val channelId = "newsUpdateEntity"

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId, "lenta_news", NotificationManager.IMPORTANCE_LOW)
            channel.description = "lenta_news"
            context.getSystemService(NotificationManager::class.java).createNotificationChannel(channel)
        }

        val notification = NotificationCompat.Builder(context, channelId)
                .setContentTitle(newsUpdateEntity.title)
                .setContentText(newsUpdateEntity.summary)
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.place_holder_2)
                .setLargeIcon(newsUpdateEntity.notificationIcon)
                .setContentIntent(pendingIntent)
                .setShowWhen(true)
                .setLocalOnly(true)
                .build()
        val notifId = 1145

        sendNotification(context, notification)

    }

    private fun sendNotification(context: Context, notification: Notification) {
        NotificationManagerCompat.from(context).notify(Random().nextInt(), notification)
    }
}