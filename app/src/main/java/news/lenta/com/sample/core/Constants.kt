package news.lenta.com.sample.core

val API_ENDPOINT =  "https://lenta.ru"
val APP_PREFERENCES = "app_preferences"

val NOTIFICATION_INTENT_EXTRA = "NotificationIntentExtra"