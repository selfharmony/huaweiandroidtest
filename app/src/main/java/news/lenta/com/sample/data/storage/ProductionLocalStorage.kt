package news.lenta.com.sample.data.storage

import android.content.SharedPreferences
import io.reactivex.Completable
import io.reactivex.Flowable
import news.lenta.com.sample.data.storage.room.dao.NewsDao
import news.lenta.com.sample.data.storage.room.model.NewsForDb
import news.lenta.com.sample.ui.model.NewsEntity
import news.lenta.com.sample.domain.mapper.NewsMapper

class ProductionLocalStorage(val prefs: SharedPreferences, val newsDao: NewsDao) : LocalStorage {

    override fun saveCurrentNews(news: List<NewsForDb>): Flowable<List<NewsForDb>> {
        return Flowable.fromCallable {
            newsDao.clear()
        }.map {
            news.forEach {
                newsDao.saveNews(it)
            }
        }.map { news }
    }

    override fun getSavedNews(): Flowable<List<NewsForDb>> {
        return Flowable.fromCallable {
            newsDao.getNews()
        }
    }
}