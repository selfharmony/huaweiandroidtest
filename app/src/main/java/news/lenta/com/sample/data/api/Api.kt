package news.lenta.com.sample.data.api

import android.graphics.Bitmap
import io.reactivex.Observable
import news.lenta.com.sample.data.api.model.NewsResponse
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Url


interface Api {

    @GET("/rss/news")
    fun getNews(): Observable<NewsResponse>

    @GET()
    fun getImage(@Url url: String): Observable<ResponseBody>
}