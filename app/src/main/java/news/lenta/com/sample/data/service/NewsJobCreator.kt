package news.lenta.com.sample.data.service

import com.evernote.android.job.Job
import com.evernote.android.job.JobCreator
import news.lenta.com.sample.domain.interactors.Interactor
import news.lenta.com.sample.ui.model.NewsUpdateEntity


class NewsJobCreator(
        val updateNewsInteractor: Interactor<Void, List<NewsUpdateEntity>>
) : JobCreator {

    override fun create(tag: String): Job? {
        when (tag) {
            NewsSyncJob.TAG -> return NewsSyncJob(updateNewsInteractor)
            else -> return null
        }
    }

}