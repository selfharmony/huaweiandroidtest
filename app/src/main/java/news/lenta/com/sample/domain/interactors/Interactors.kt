package news.lenta.com.sample.domain.interactors

import android.graphics.Bitmap
import io.reactivex.Flowable
import io.reactivex.Scheduler
import io.reactivex.functions.BiFunction
import news.lenta.com.sample.data.api.model.NewsResponse
import news.lenta.com.sample.data.repository.Repository
import news.lenta.com.sample.ui.model.NewsEntity
import okhttp3.ResponseBody
import java.util.concurrent.Executor
import android.graphics.BitmapFactory
import com.evernote.android.job.JobCreator
import news.lenta.com.sample.domain.mapper.NewsMapper
import news.lenta.com.sample.ui.model.NewsUpdateEntity
import java.io.BufferedInputStream


open class GetNewsInteractor(threadScheduler: Executor, uiScheduler: Scheduler, repo: Repository)
    : BaseInteractor<Void, List<NewsEntity>>(threadScheduler, uiScheduler, repo) {
    override fun buildObservable(): Flowable<List<NewsEntity>> {
        return repo.getNews()
                .map {
                    NewsMapper.fromNetwork(it)
                }
                .flatMap { repo.saveNewsToDb(it) } // если закомментить - при запуске покажет нотификацию и не будет сохранять в базу
    }
}

open class GetNewsUpdateInteractor(threadScheduler: Executor, uiScheduler: Scheduler, repo: Repository)
    : BaseInteractor<Void, List<NewsUpdateEntity>>(threadScheduler, uiScheduler, repo) {
    override fun buildObservable(): Flowable<List<NewsUpdateEntity>> {
        return repo.getNews()
                .map {
                    NewsMapper.fromNetwork(it)
                }
                .zipWith(repo.getNewsFromDb(), BiFunction<List<NewsEntity>, List<NewsEntity>, List<NewsEntity>>() { news, stored ->
                    var updated = mutableListOf<NewsEntity>()
                    news.filter { n ->
                        !stored.contains(n)
                    }
                })
                .concatMapIterable { newsItem -> newsItem }
                .flatMap {
                    Flowable.zip(
                            Flowable.just(it),
                            repo.getNotificationIcon(it.imageUrl),
                            BiFunction<NewsEntity, ResponseBody, NewsUpdateEntity> { newsItem, bitmapResponseBody ->
                                val stream = bitmapResponseBody.byteStream()
                                val bitmap = BitmapFactory.decodeStream(stream) ?: Bitmap.createBitmap(200, 200, Bitmap.Config.ARGB_8888);
                                NewsUpdateEntity(
                                        title = newsItem.title,
                                        summary = newsItem.summary,
                                        articleUrl = newsItem.articleUrl,
                                        publishedDate = newsItem.publishedDate,
                                        imageUrl = newsItem.imageUrl,
                                        notificationIcon = bitmap
                                )
                            })
                }
                .toList()
                .toFlowable()

    }
}
