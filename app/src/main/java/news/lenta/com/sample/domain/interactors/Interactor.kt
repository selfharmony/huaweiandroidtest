package news.lenta.com.sample.domain.interactors

import io.reactivex.Flowable
import io.reactivex.subscribers.DisposableSubscriber

interface Interactor<T, R>{
    fun execute(subscriber: DisposableSubscriber<R>)
    fun updateParameter(parameter: T)
    fun dispose()
    fun getObservable(): Flowable<R>
    fun isDisposed(): Boolean
}