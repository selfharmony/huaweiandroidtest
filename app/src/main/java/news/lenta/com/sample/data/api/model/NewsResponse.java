package news.lenta.com.sample.data.api.model;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name = "rss", strict = false)
public class NewsResponse {
    @Element(name = "channel")
    public Channel channel;

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    @Root(name = "channel", strict = false)
    public static class Channel {

        @ElementList(inline = true, name = "item")
        private List<Item> item;

        public List<Item> getItem() {
            return item;
        }

        public void setItem(List<Item> item) {
            this.item = item;
        }

        public static class Item {

            @Element(name = "guid", required = false)
            private String guid;

            @Element(name = "title", required = false)
            private String title;

            @Element(name = "link", required = false)
            private String link;

            @Element(name = "description", required = false)
            private String description;

            @Element(name = "pubDate", required = false)
            private String pubDate;

            @Element(name = "enclosure", required = false)
            private Enclosure enclosure;

            public String getGuid() {
                return guid;
            }

            public void setGuid(String guid) {
                this.guid = guid;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getLink() {
                return link;
            }

            public void setLink(String link) {
                this.link = link;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getPubDate() {
                return pubDate;
            }

            public void setPubDate(String pubDate) {
                this.pubDate = pubDate;
            }

            public Enclosure getEnclosure() {
                return enclosure;
            }

            public void setEnclosure(Enclosure enclosure) {
                this.enclosure = enclosure;
            }

            public static class Enclosure {

                @Attribute(name = "url", required = false)
                private String url;

                @Attribute(name = "length", required = false)
                private long length;

                @Attribute(name = "type", required = false)
                private String type;

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public long getLength() {
                    return length;
                }

                public void setLength(long length) {
                    this.length = length;
                }

                public String getType() {
                    return type;
                }

                public void setType(String type) {
                    this.type = type;
                }
            }

        }

    }


}
