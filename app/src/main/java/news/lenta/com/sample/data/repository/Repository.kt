package news.lenta.com.sample.data.repository

import android.graphics.Bitmap
import com.evernote.android.job.JobCreator
import io.reactivex.Flowable
import news.lenta.com.sample.data.api.model.NewsResponse
import news.lenta.com.sample.data.storage.room.model.NewsForDb
import news.lenta.com.sample.ui.model.NewsEntity
import okhttp3.ResponseBody

interface Repository{
    fun getNews(): Flowable<NewsResponse>

    fun getNotificationIcon(url: String): Flowable<ResponseBody>


    fun saveNewsToDb(news: List<NewsEntity>): Flowable<List<NewsEntity>>

    fun getNewsFromDb(): Flowable<List<NewsEntity>>

}