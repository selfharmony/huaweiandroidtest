package news.lenta.com.sample.data.storage

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import news.lenta.com.sample.data.storage.room.model.NewsForDb
import news.lenta.com.sample.ui.model.NewsEntity

interface LocalStorage {

    fun saveCurrentNews(news: List<NewsForDb>): Flowable<List<NewsForDb>>
    fun getSavedNews(): Flowable<List<NewsForDb>>


}

