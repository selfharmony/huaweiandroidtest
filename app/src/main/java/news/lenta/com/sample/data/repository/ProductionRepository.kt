package news.lenta.com.sample.data.repository

import android.graphics.Bitmap
import com.evernote.android.job.JobCreator
import com.evernote.android.job.JobManager
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.internal.operators.flowable.FlowableElementAt
import news.lenta.com.sample.data.api.NetworkClient
import news.lenta.com.sample.data.storage.LocalStorage
import news.lenta.com.sample.data.storage.room.model.NewsForDb
import news.lenta.com.sample.ui.model.NewsEntity
import news.lenta.com.sample.domain.mapper.NewsMapper

class ProductionRepository(val networkClient: NetworkClient,
                           val localStorage: LocalStorage
) : Repository {

    override fun getNews() = networkClient.getNews()

    override fun getNotificationIcon(url: String) = networkClient.getNotificationIcon(url)

    override fun saveNewsToDb(news: List<NewsEntity>): Flowable<List<NewsEntity>> {
        return Flowable.defer {
            val mappedNews = NewsMapper.toDb(news)
            localStorage.saveCurrentNews(mappedNews)
        }.map { news }

    }

    override fun getNewsFromDb(): Flowable<List<NewsEntity>> {
        return localStorage.getSavedNews()
                .map { NewsMapper.fromDb(it) }
    }

}