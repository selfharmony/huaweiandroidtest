package news.lenta.com.sample.data.storage.room.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import news.lenta.com.sample.data.storage.room.Tables
import news.lenta.com.sample.data.storage.room.model.NewsForDb


@Dao
interface NewsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveNews(user: NewsForDb)

    @Query("SELECT * FROM ${Tables.NEWS}")
    fun getNews(): List<NewsForDb>

    @Query("DELETE FROM ${Tables.NEWS}")
    fun clear()
}