package news.lenta.com.sample.ui.fragments.news

import android.content.Context
import android.os.Bundle
import com.evernote.android.job.JobCreator
import news.lenta.com.sample.ui.base.BaseContract
import news.lenta.com.sample.ui.model.NewsEntity
import news.lenta.com.sample.ui.model.NewsUpdateEntity

interface Contract {
    interface view : BaseContract.View {
        fun showNews(news: List<NewsEntity>)
        fun navigateToDetail(bundle: Bundle)

    }
    interface presenter : BaseContract.Presenter{
        fun onItemClicked(pos: Int, item: NewsEntity)
        fun getNews()

        fun scheduleJob()
    }
}