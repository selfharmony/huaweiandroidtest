package news.lenta.com.sample.data.api.deserialize

import news.lenta.com.sample.data.api.deserialize.TestData.TEST_JSON_OBJECT_WITH_MULTIMEDIA
import news.lenta.com.sample.data.api.deserialize.TestData.TEST_JSON_OBJECT_WITH_MULTIMEDIA_LIST
import news.lenta.com.sample.data.api.deserialize.TestData.TEST_JSON_OBJECT_WITH_STRING
import news.lenta.com.sample.data.api.deserialize.TestData.TEST_KEY
import news.lenta.com.sample.data.api.deserialize.TestData.TEST_MULTIMEDIA_LIST
import news.lenta.com.sample.data.api.deserialize.TestData.TEST_JSON_OBJECT_WITH_STRING_LIST
import news.lenta.com.sample.data.api.deserialize.TestData.TEST_STRING_LIST
import news.lenta.com.sample.data.api.deserialize.TestData.TEST_STRING
import news.lenta.com.sample.data.api.model.MediaItem
import org.junit.Assert
import org.junit.Before
import org.junit.Test

// Because during the refactoring MediaEntity and NewsEntity became simple data objects
// and more complex serialization logic was delegated to NewsItemDeserializer,
// their tests are now here

class NewsItemDeserializerTest {
    lateinit var newsItemDeserializer: NewsItemDeserializer

    @Before
    @Throws(Exception::class)
    fun setUp() {
        newsItemDeserializer = NewsItemDeserializer()
    }

    @Test
    fun shouldDeserializeSimpleString(){
        val simpleString = newsItemDeserializer.readToSimpleString(TEST_JSON_OBJECT_WITH_STRING, TEST_KEY)
        Assert.assertEquals(TEST_STRING, simpleString)
    }

    @Test
    fun shouldDeserializeStringList(){
        val stringList: List<String?>? = newsItemDeserializer.readToStringsList(TEST_JSON_OBJECT_WITH_STRING_LIST, TEST_KEY)
        Assert.assertEquals(TEST_STRING_LIST, stringList)
    }

    @Test
    fun shouldConvertStringToStringList(){
        val stringList: List<String?>? = newsItemDeserializer.readToStringsList(TEST_JSON_OBJECT_WITH_STRING, TEST_KEY)
        Assert.assertEquals(TEST_STRING_LIST, stringList)
    }

    @Test
    fun shouldDeserializeMultimediaList(){
        val stringList: List<MediaItem?>? = newsItemDeserializer.readMultimediaList(TEST_JSON_OBJECT_WITH_MULTIMEDIA_LIST, TEST_KEY)
        Assert.assertEquals(TEST_MULTIMEDIA_LIST, stringList)
    }

    @Test
    fun shouldConvertMultimediaToMultimediaList(){
        val stringList: List<MediaItem?>? = newsItemDeserializer.readMultimediaList(TEST_JSON_OBJECT_WITH_MULTIMEDIA, TEST_KEY)
        Assert.assertEquals(TEST_MULTIMEDIA_LIST, stringList)
    }
}